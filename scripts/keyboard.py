#!/usr/bin/env python
import rospy

from geometry_msgs.msg import Twist
from std_msgs.msg import String

import math

global control_cmd

def key_down(msg):

    #print(msg.data)
    xyz = 0.01
    rotate = 0.1
    if msg.data == "w":
        control_cmd.linear.x += xyz
    elif msg.data == "s":
        control_cmd.linear.x -= xyz
    elif msg.data == "d":
        control_cmd.linear.y -= xyz
    elif msg.data == "a":
        control_cmd.linear.y += xyz
    elif msg.data == "z":
        control_cmd.linear.z -= xyz
    elif msg.data == "c":
        control_cmd.linear.z += xyz
    elif msg.data == "e":
        control_cmd.angular.z -= rotate
    elif msg.data == "q":
        control_cmd.angular.z += rotate
    elif msg.data == " ":
        control_cmd.linear.x = 0
        control_cmd.linear.y = 0
        control_cmd.linear.z = 0
        control_cmd.angular.z = 0

def key_up(msg):
    return
    
if __name__ == "__main__":
    rospy.init_node('keyboard_controller')

    control_topic_name = '/cmd_vel'

    key_down_sub = rospy.Subscriber("/keyboard_down", String, key_down)
    key_up_sub = rospy.Subscriber("/keyboard_up", String, key_up)

    control_pub = rospy.Publisher(control_topic_name, Twist, queue_size=1)

    control_cmd = Twist()

    # how many times in a second the control loop is going to run
    ros_rate = rospy.Rate(10)

    while not rospy.is_shutdown():

        control_pub.publish(control_cmd)

        ros_rate.sleep()
e
