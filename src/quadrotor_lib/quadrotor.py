import rospy
import math
import time
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from std_srvs.srv import Trigger
from gazebo_msgs.srv import GetModelState
from quadrotor_description.srv import LaserSensor, CameraSensor
class Robot:
    def __init__(self, robot_name):
        rospy.init_node("quadrotor_controller")

        self.quadrotor_cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=10)

        self.last_twist_cmd = Twist()
        self.last_twist_cmd.linear.x = 0
        self.last_twist_cmd.linear.y = 0
        self.last_twist_cmd.linear.z = 0
        self.last_twist_cmd.angular.x = 0
        self.last_twist_cmd.angular.y = 0
        self.last_twist_cmd.angular.z = 0

        self._init_services()
        self._init_game_controller()


    def _init_services(self):
        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_quadrotor_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
        except:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/front_camera/camera_service", 1.0)
            self.get_front_camera = rospy.ServiceProxy("/front_camera/camera_service", CameraSensor)

            resp = self.get_front_camera()
            self.camera_height = resp.height
            self.camera_width = resp.width
            
        except:
            rospy.logerr("Service call failed: %s" % (e,))

#        try:
#            rospy.wait_for_service("/quadrotor/get_front_range", 1.0)
#            self.get_front_dist_sensor = rospy.ServiceProxy("/quadrotor/get_front_range", LaserSensor)
#        except:
#            rospy.logerr("Service call failed: %s" % (e,))


    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)

            resp = self.robot_handle()
            self.status = resp.message

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"

            rospy.logerr("Service call failed: %s" % (e,))

    def _check_game_controller(self):
        resp = self.robot_handle()
        self.status = resp.message


    def move(self, speed_x, speed_y, speed_z):
        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "Stop":
                return

            self.last_twist_cmd.linear.x = speed_x
            self.last_twist_cmd.linear.y = speed_y
            self.last_twist_cmd.linear.z = speed_z

            self.quadrotor_cmd_vel.publish(self.last_twist_cmd)

    def rotate(self, speed_yaw):
        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "Stop":
                return

            self.last_twist_cmd.angular.z = speed_yaw

            self.quadrotor_cmd_vel.publish(self.last_twist_cmd)


    def get_latest_coordinates(self):
        pass

    def get_camera_data(self):
        resp = self.get_front_camera()

        image_rgb_data = []

        for i in range(self.camera_height * self.camera_width * 3):
            image_rgb_data.append(ord(resp.data[i]))

        return image_rgb_data

    @property
    def altitude(self):
        resp = self.get_quadrotor_state('quadrotor', '')

        return resp.pose.position.z

#    @property
#    def front_sensor(self):
#        resp = self.get_front_dist_sensor()
#
#        return resp.data

    def is_ok(self):
        if not rospy.is_shutdown():
            rospy.sleep(0.05)
            return True
        else:
            return False
